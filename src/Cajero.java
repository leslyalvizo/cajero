import java.util.Scanner;

public class Cajero {
	private static Scanner sc;

	public static void main(String [] arg){
		System.out.println("Introduce la cantidad a retirar: $");
		sc = new Scanner (System.in);
		try{
			Integer cantidad = sc.nextInt();
			if(cantidad<=7000) {
				System.out.println("Tome su dinero: ");
				calculaDenom(cantidad);
			}else {
				System.out.println("El monto m�ximo de retiro son $7000");
			}
		}catch(Exception e) {
			System.out.println("Cantidad inv�lida ");
			System.out.println("Error: " + e.getStackTrace());
		}
		
	}
	
	public static void calculaDenom(Integer cantidad){
		if(cantidad >= 500) {
			cantidad = imprime(cantidad,500);
		}
		if(cantidad >= 200) {
			cantidad = imprime(cantidad,200);
		}
		if(cantidad >= 100) {
			cantidad = imprime(cantidad,100);
		}
		if(cantidad >= 50) {
			cantidad = imprime(cantidad,50);
		}
		if(cantidad >= 20) {
			cantidad = imprime(cantidad,20);
		}
		if(cantidad >= 10) {
			cantidad = imprimeMoneda(cantidad,10);
		}
		if(cantidad >= 5) {
			cantidad = imprimeMoneda(cantidad,5);
		}
		if(cantidad >= 2) {
			cantidad = imprimeMoneda(cantidad,2);
		}
		if(cantidad >= 1) {
			cantidad = imprimeMoneda(cantidad,1);
		}
	}
	
	public static Integer imprime(Integer cantidad, Integer valor) {
		Integer contador = cantidad / valor;
		cantidad = cantidad % valor;
		if (contador > 1) {
			System.out.println(contador + " billetes de $"+valor);
		}else {
			System.out.println(contador + " billete de $"+valor);
		}
		return cantidad;
	}
	
	public static Integer imprimeMoneda(Integer cantidad, Integer valor) {
		Integer contador = cantidad / valor;
		cantidad = cantidad % valor;
		if (contador > 1) {
			System.out.println(contador + " monedas de $"+valor);
		}else {
			System.out.println(contador + " moneda de $"+valor);
		}
		return cantidad;
	}

}
